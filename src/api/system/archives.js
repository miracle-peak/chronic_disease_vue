import request from '@/utils/request'

// 查询家庭档案信息列表
export function listArchives(query) {
  return request({
    url: '/system/archives/list',
    method: 'get',
    params: query
  })
}

// 查询家庭档案信息详细
export function getArchives(familyCode) {
  return request({
    url: '/system/archives/' + familyCode,
    method: 'get'
  })
}

// 新增家庭档案信息
export function addArchives(data) {
  return request({
    url: '/system/archives',
    method: 'post',
    data: data
  })
}
// 修改家庭成员参合状态
export function updateState(idCard) {
  return request({
    url: '/system/member/updateState',
    method: 'get',
    data: {
      "idCard":idCard
    }
  })
}

// 修改家庭档案信息
export function updateArchives(data) {
  return request({
    url: '/system/archives',
    method: 'put',
    data: data
  })
}

// 删除家庭档案信息
export function delArchives(familyCode) {
  return request({
    url: '/system/archives/' + familyCode,
    method: 'delete'
  })
}

// 导出家庭档案信息
export function exportArchives(query) {
  return request({
    url: '/system/archives/export',
    method: 'get',
    params: query
  })
}
