import request from '@/utils/request'

// 查询行政区域管理列表
export function listArea(query) {
  return request({
    url: '/system/area/list',
    method: 'get',
    params: query
  })
}

// 查询行政区域管理列表
export function listAll(query) {
  return request({
    url: '/system/area/all',
    method: 'get',
    params: query
  })
}

// 查询行政区域管理详细
export function getArea(areaCode) {
  return request({
    url: '/system/area/' + areaCode,
    method: 'get'
  })
}

// 新增行政区域管理
export function addArea(data) {
  return request({
    url: '/system/area',
    method: 'post',
    data: data
  })
}

// 修改行政区域管理
export function updateArea(data) {
  return request({
    url: '/system/area',
    method: 'put',
    data: data
  })
}

// 删除行政区域管理
export function delArea(areaCode) {
  return request({
    url: '/system/area/' + areaCode,
    method: 'delete'
  })
}

// 导出行政区域管理
export function exportArea(query) {
  return request({
    url: '/system/area/export',
    method: 'get',
    params: query
  })
}
