import request from '@/utils/request'

// 查询慢性政策管理列表
export function listPolicy(query) {
  return request({
    url: '/system/policy/list',
    method: 'get',
    params: query
  })
}

// 查询慢性政策管理详细
export function getPolicy(year) {
  return request({
    url: '/system/policy/' + year,
    method: 'get'
  })
}

// 新增慢性政策管理
export function addPolicy(data) {
  return request({
    url: '/system/policy',
    method: 'post',
    data: data
  })
}

// 修改慢性政策管理
export function updatePolicy(data) {
  return request({
    url: '/system/policy',
    method: 'put',
    data: data
  })
}

// 删除慢性政策管理
export function delPolicy(id) {
  return request({
    url: '/system/policy/' + id,
    method: 'delete'
  })
}

// 导出慢性政策管理
export function exportPolicy(query) {
  return request({
    url: '/system/policy/export',
    method: 'get',
    params: query
  })
}
