import request from '@/utils/request'

// 查询农合经办点列表
export function listInstitution(query) {
  return request({
    url: '/system/institution/list',
    method: 'get',
    params: query
  })
}

// 查询农合经办点详细
export function getInstitution(institutionCode) {
  return request({
    url: '/system/institution/' + institutionCode,
    method: 'get'
  })
}

// 新增农合经办点
export function addInstitution(data) {
  return request({
    url: '/system/institution',
    method: 'post',
    data: data
  })
}

// 修改农合经办点
export function updateInstitution(data) {
  return request({
    url: '/system/institution',
    method: 'put',
    data: data
  })
}

// 删除农合经办点
export function delInstitution(institutionCode) {
  return request({
    url: '/system/institution/' + institutionCode,
    method: 'delete'
  })
}

// 导出农合经办点
export function exportInstitution(query) {
  return request({
    url: '/system/institution/export',
    method: 'get',
    params: query
  })
}