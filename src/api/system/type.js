import request from '@/utils/request'

// 查询慢性病分类管理列表
export function listType(query) {
  return request({
    url: '/system/type/list',
    method: 'get',
    params: query
  })
}

// 查询慢性病分类管理详细
export function getType(id) {
  return request({
    url: '/system/type/' + id,
    method: 'get'
  })
}

// 新增慢性病分类管理
export function addType(data) {
  return request({
    url: '/system/type',
    method: 'post',
    data: data
  })
}

// 修改慢性病分类管理
export function updateType(data) {
  return request({
    url: '/system/type',
    method: 'put',
    data: data
  })
}

// 删除慢性病分类管理
export function delType(id) {
  return request({
    url: '/system/type/' + id,
    method: 'delete'
  })
}

// 导出慢性病分类管理
export function exportType(query) {
  return request({
    url: '/system/type/export',
    method: 'get',
    params: query
  })
}