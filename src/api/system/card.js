import request from '@/utils/request'

// 查询慢性病证管理列表
export function listCard(query) {
  return request({
    url: '/system/card/list',
    method: 'get',
    params: query
  })
}

// 查询慢性病证管理详细
export function getCard(illCardNo) {
  return request({
    url: '/system/card/' + illCardNo,
    method: 'get'
  })
}

// 新增慢性病证管理
export function addCard(data) {
  return request({
    url: '/system/card',
    method: 'post',
    data: data
  })
}

// 修改慢性病证管理
export function updateCard(data) {
  return request({
    url: '/system/card',
    method: 'put',
    data: data
  })
}

// 删除慢性病证管理
export function delCard(illCardNo) {
  return request({
    url: '/system/card/' + illCardNo,
    method: 'delete'
  })
}

// 导出慢性病证管理
export function exportCard(query) {
  return request({
    url: '/system/card/export',
    method: 'get',
    params: query
  })
}