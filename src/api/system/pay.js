import request from '@/utils/request'

// 查询参合缴费登记列表
export function listPay(query) {
  return request({
    url: '/system/pay/list',
    method: 'get',
    params: query
  })
}

/**
 * 查询家庭未参合成员
 *
 * @param familyCode
 */
export function noJoin(familyCode) {
  return request({
    url: '/system/member/noJoin',
    method: 'get',
    params: {
      "familyCode":familyCode
    }
  })
}

// 查询身份证号参合
export function getOne(joinIdCard) {
  return request({
    url: '/system/pay/getOne',
    method: 'get',
    params: {
      "joinIdCard":joinIdCard
    }
  })
}

// 查询参合缴费登记详细
export function getPay(joinNhCode) {
  return request({
    url: '/system/pay/' + joinNhCode,
    method: 'get'
  })
}

// 新增参合缴费登记
export function addPay(data) {
  return request({
    url: '/system/pay',
    method: 'post',
    data: data
  })
}

// 修改参合缴费登记
export function updatePay(data) {
  return request({
    url: '/system/pay',
    method: 'put',
    data: data
  })
}

// 删除参合缴费登记
export function delPay(joinNhCode) {
  return request({
    url: '/system/pay/' + joinNhCode,
    method: 'delete'
  })
}

// 导出参合缴费登记
export function exportPay(query) {
  return request({
    url: '/system/pay/export',
    method: 'get',
    params: query
  })
}
